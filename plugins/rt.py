import time
import net.mapserv as mapserv
import net.charserv as charserv
import net.stats as stats
import commands
import logicmanager
import status
import plugins
import itemdb
# ~ import random
from collections import deque
from net.inventory import get_item_index, get_storage_index
from utils import extends
from actor import find_nearest_being
from chat import send_whisper as whisper

from net.onlineusers import OnlineUsers

__all__ = [ 'PLUGIN', 'init' ]

PLUGIN = {
    'name': 'randomthings',
    'requires': ['chatbot'],
    'blocks': (),
}

#FIXME This must be in a library

def preloadArray(nfile):
    try:
        file = open(nfile, "r")
        array=[]
        for x in file.readlines():
            x = x.replace("\n", "")
            x = x.replace("\r", "")
            array.append(x)
        file.close()
        return array
    except:
        print "preloadArray: File " +  nfile + " not found!"

ignored_players     = preloadArray("config/ignored.txt")

def palDetect(nick, message, is_whisper, match):
    outmsg = '' # We cannot use print nor whisper faster too many times (bad, ban!) so we have to "assemble" a single message to send
    for element in message.split(' '): # Not 100% perfect since !<command> got included too
        palindrome=0
        i=0
        # while I'm pointing at the element before the middle and that one is equal to the last one less it's position
        while i < len(element)/2 and element[i]==element[len(element)-1-i]:
            palindrome+=1
            i+=1
        # A palindrome has same number of occurrences as the amount of half the letters (usually when even in length)
        if palindrome==len(element)/2:
            #print element + " is a palindrome"
            outmsg += element + " "
    whisper(nick, outmsg) # We cannot use print since it will print messages on terminal not on whisper
    time.sleep(3)

rt_commands = {
    '!pal (.*)' : palDetect,
}

def init(config):

    for cmd, action in rt_commands.items():
        plugins.chatbot.add_command(cmd, action)
