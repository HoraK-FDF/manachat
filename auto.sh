#!/bin/bash
while :
do
     echo "Loading manachat (abort with CTRL+C)"
     # Increase nice number in order to be the last served by kernel
     nice -n 19 python simple.py
     sleep 60
done
