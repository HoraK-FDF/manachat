#!/bin/bash

# this script merges new items into an existing shoplist and keeps the current values

ITEM_DB_DIR="/mnt/E/TMW/git.themanaworld.org/#HoraK-FDF/legacy/serverdata/world/map/db"
SHOP_LIST="shoplist.txt"
CURRENT_SHOP_LIST="shoplist.txt"

SORT_MODE="1"
# 0 = no sort
# 1 = sort by id
# 2 = sort by item name

if [ ! -e "$ITEM_DB_DIR" ]; then
 echo "warning the directory \""$ITEM_DB_DIR"\" does not exist ..."
 echo "... exiting!"
 exit 1
fi

if [ -e "$SHOP_LIST" ]; then
 echo "warning the file "$SHOP_LIST" allready exists ..."
 echo "... enter y/Y to overwrite it : "
 read KEY
 if [ ! $KEY = "y" ] && [ ! $KEY = "Y" ]; then
  echo "... exiting!"
  exit 0
 fi
fi

NEW_ITEMS=`cat "$ITEM_DB_DIR"/item_db* | grep -o -E "^[1-9][0-9]*[,][ ]+[a-zA-Z]*[,]" | sed -r -e 's/[,][ ]*/ /g' -e 's/[ ]*$//g' -e 's/ / 0 0 0 0 /g'`
OLD_ITEMS=`cat "$CURRENT_SHOP_LIST"`

OLD_IFS=$IFS
IFS=$'\n'

FIRST_CYCLE=1

for i in $NEW_ITEMS; do
 ITEM_ID=`echo $i | cut -d ' ' -f 1`
 IS_IN_OLD_ITEMS=`echo "$OLD_ITEMS" | grep -E "^"$ITEM_ID"[ ]"`
 if [ ! "$IS_IN_OLD_ITEMS" = "" ]; then
  # item_id  buy_amount buy_price sell_amount sell_price comment/itemname(optional)
  BUY_AMOUNT=`echo "$IS_IN_OLD_ITEMS" | cut -d ' ' -f 2`
  BUY_PRICE=`echo "$IS_IN_OLD_ITEMS" | cut -d ' ' -f 3`
  SELL_AMOUNT=`echo "$IS_IN_OLD_ITEMS" | cut -d ' ' -f 4`
  SELL_PRICE=`echo "$IS_IN_OLD_ITEMS" | cut -d ' ' -f 5`
  ITEM_NAME=`echo "$i" | cut -d ' ' -f 6`
  MERGED_ITEMS=""$MERGED_ITEMS""$ITEM_ID" "$BUY_AMOUNT" "$BUY_PRICE" "$SELL_AMOUNT" "$SELL_PRICE" "$ITEM_NAME""$'\n'
 else
  MERGED_ITEMS=""$MERGED_ITEMS""$i""$'\n'
 fi
done

IFS=$OLD_IFS

case $SORT_MODE in
 0)
  echo -n "$MERGED_ITEMS" > "$SHOP_LIST"
 ;;
 1)
  echo -n "$MERGED_ITEMS" | sort -t " " -k 1 -g > "$SHOP_LIST"
 ;;
 2)
  echo -n "$MERGED_ITEMS" | sort -t " " -k 2 > "$SHOP_LIST"
 ;;
 *)
  echo "wrong value for variable SORT_MODE!"
  exit 1
esac

exit 0
