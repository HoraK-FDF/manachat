#!/bin/bash

# this script creates a new shoplist and does not keep current values

ITEM_DB_DIR=""
SHOP_LIST="shoplist.txt"

SORT_MODE="1"
# 0 = no sort
# 1 = sort by id
# 2 = sort by item name

if [ ! -e "$ITEM_DB_DIR" ]; then
 echo "warning the directory \""$ITEM_DB_DIR"\" does not exist ..."
 echo "... exiting!"
 exit 1
fi

if [ -e "$SHOP_LIST" ]; then
 echo "warning the file "$SHOP_LIST" allready exists ..."
 echo "... enter y/Y to overwrite it : "
 read KEY
 if [ ! $KEY = "y" ] && [ ! $KEY = "Y" ]; then
  echo "... exiting!"
  exit 0
 fi
fi

case $SORT_MODE in
 0)
  cat "$ITEM_DB_DIR"/item_db* | grep -o -E "^[1-9][0-9]*[,][ ]+[a-zA-Z]*[,]" | sed -r -e 's/[,][ ]*/ /g' -e 's/[ ]*$//g' -e 's/ / 0 0 0 0 /g' > "$SHOP_LIST"
 ;;
 1)
  cat "$ITEM_DB_DIR"/item_db* | grep -o -E "^[1-9][0-9]*[,][ ]+[a-zA-Z]*[,]" | sed -r -e 's/[,][ ]*/ /g' -e 's/[ ]*$//g' -e 's/ / 0 0 0 0 /g' | sort -t " " -k 1 -g > "$SHOP_LIST"
 ;;
 2)
  cat "$ITEM_DB_DIR"/item_db* | grep -o -E "^[1-9][0-9]*[,][ ]+[a-zA-Z]*[,]" | sed -r -e 's/[,][ ]*/ /g' -e 's/[ ]*$//g' -e 's/ / 0 0 0 0 /g' | sort -t " " -k 2 > "$SHOP_LIST"
 ;;
 *)
  echo "wrong value for variable SORT_MODE!"
  exit 1
esac

exit 0
