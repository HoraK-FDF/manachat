#!/bin/bash

SHOP_LIST_FILE="shoplist.txt"
TMP_FILE="shoplist_tmp.txt"
# if in item db directory dont use "item_db" at start of filename

SORT_MODE="0"
# 0 = sort by id
# 1 = sort by item name

case $SORT_MODE in
 0)
  cat "$SHOP_LIST_FILE" | sort -t " " -k 1 -g > "$TMP_FILE"
  mv "$TMP_FILE" "$SHOP_LIST_FILE"
 ;;
 1)
  cat "$SHOP_LIST_FILE" | sort -t " " -k 2 > "$TMP_FILE"
  mv "$TMP_FILE" "$SHOP_LIST_FILE"
 ;;
 *)
  echo "wrong value for variable SORT_MODE!"
  exit 1
esac

exit 0
